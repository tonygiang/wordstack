﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/VariableFloat")]
public class VariableFloat : ScriptableObject
{
  [SerializeField] protected float _value = 0;
  public float Value
  {
    get => _value;
    set
    {
      var oldValue = _value;
      bool hasChange = !oldValue.Equals(value);
      _value = value;
      if (hasChange) OnChange.Listeners.Invoke(value);
    }
  }
  public EventFloatSO OnChange = null;

  public void Set(VariableFloat variable) => Value = variable.Value;
}
