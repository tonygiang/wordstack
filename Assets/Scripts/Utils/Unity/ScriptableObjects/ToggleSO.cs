﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "ScriptableObjects/ToggleSO")]
public class ToggleSO : ScriptableObject
{
  public Toggle Value = null;
}
