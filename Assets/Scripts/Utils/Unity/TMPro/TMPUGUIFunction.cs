﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Utils;

public class TMPUGUIFunction : ExtensionBehaviour<TextMeshProUGUI>
{
  public string Template = "{0}";
  public string FormatString = string.Empty;
  public SerializableNumberFormatInfo FormatInfo
    = new SerializableNumberFormatInfo();
  public int CurrentBoundInt = 0;
  public float TransitionSeconds = 0;

  public void SetInt(int number)
  {
    CurrentBoundInt = number;
    BaseComp.text = string.Format(Template,
      number.ToString(FormatString, FormatInfo));
  }

  public void SetInt(object number) => SetInt((int)number);

  public void TransitionToInt(int newInt)
  { StopAllCoroutines(); StartCoroutine(UpdateText(newInt)); }

  public void TransitionToInt(object newInt) => TransitionToInt((int)newInt);

  IEnumerator UpdateText(int endValue)
  {
    var initialBoundInt = CurrentBoundInt;
    var secondsElapsed = 0f;
    do
    {
      Mathf.Lerp(initialBoundInt, endValue, secondsElapsed / TransitionSeconds)
        .PassTo(Mathf.RoundToInt)
        .PassTo(SetInt);
      yield return Utils.Unity.Constants.WaitForFixedUpdate;
      secondsElapsed += Time.fixedDeltaTime;
    } while (secondsElapsed < TransitionSeconds);
    SetInt(endValue);
    yield break;
  }
}
