﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[DisallowMultipleComponent]
[RequireComponent(typeof(TMP_InputField))]
public class ValidIfMatchInputField : ClientsideValidator<TMP_InputField>
{
  [SerializeField] TMP_InputField InputFieldToMatch = null;
  [SerializeField] bool ValidIfTextDifferentInstead = false;

  void Start() { }

  public override bool Validate() =>
    (InputFieldToMatch.text == BaseComp.text) ^ ValidIfTextDifferentInstead;
}
