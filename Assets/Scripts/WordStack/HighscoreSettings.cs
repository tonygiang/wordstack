﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.XPath;
using System.Xml;
using System.Linq;
using Utils;
using System;

namespace WordStack
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/HighscoreSettings")]
  public class HighscoreSettings : ScriptableObject
  {
    public XMLDataFileSO HighscoreFile = null;
    public string HighscoreParentQuery = string.Empty;
    XmlNode HighscoreParent => HighscoreFile.Document
      .SelectSingleNode(HighscoreParentQuery);
    public string ScoreNodeTemplate = string.Empty;
    public string ScoreTimeFormat = "yyyy-MM-dd hh:mm:ss";

    public void AddScore(VariableInt score)
    {
      string.Format(ScoreNodeTemplate,
        score.Value,
        DateTime.Now.ToString(ScoreTimeFormat))
        .ImportTo(HighscoreFile.Document)
        .PassTo(HighscoreParent.AppendChild);
      HighscoreFile.Save();
    }
  }
}
