﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace WordStack.Menu
{
  public class ScoreItem : MonoBehaviour
  {
    public TextMeshProUGUI RankText = null;
    public TextMeshProUGUI ScoreText = null;
    public TextMeshProUGUI TimestampText = null;
  }
}
