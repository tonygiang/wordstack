﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using MyBox;
using Lean.Transition;
using UnityEngine.EventSystems;

namespace WordStack.Stacking.Knob
{
  public class CloneMode : RuntimeExtensionBehaviour<Knob>
  {
    public Knob Original = null;

    protected override void Awake()
    { base.Awake(); BaseComp.RB2D.gravityScale = 0f; }

    void OnEnable() { gameObject.layer = BaseComp.Settings.CloneModeLayer; }

    void OnDestroy()
    { BaseComp.RB2D.gravityScale = Original.RB2D.gravityScale; }

    void OnMouseUpAsButton()
    {
      if (EventSystem.current.IsPointerOverGameObject()) return;
      BaseComp.SceneConfigs.WordSettings.Remove(this);
      BaseComp.AudioEmitter.PlayClip(BaseComp.Settings.UnpickClip);
      this.ReturnToOriginal();
    }
  }
}

namespace WordStack.Stacking
{
  public static partial class Extensions
  {
    public static Knob.CloneMode Init(this Knob.CloneMode source,
      Knob.Knob original)
    {
      source.Original = original;
      source.BaseComp.CopyFrom(original);
      source.BaseComp.transform.position = original.transform.position;
      source.Original.ToggleDarkLayer(true);
      return source;
    }

    public static void ReturnToOriginal(this Knob.CloneMode source)
    {
      source.BaseComp.Sprite.material.colorTransition("_BaseColor",
        source.BaseComp.Sprite.material.color.WithAlphaSetTo(1f),
        0.25f,
        LeanEase.Decelerate);
      source.transform.positionTransition(source.Original.transform.position,
        0.25f,
        LeanEase.Decelerate)
        .JoinTransition()
        .EventTransition(() =>
        {
          source.gameObject.SetActive(false);
          source.Original.GetOrAddComponent<Knob.CloneOnMouseUpAsButton>();
          source.Original.ToggleDarkLayer(false);
          UnityEngine.Object.Destroy(source);
        }, 0f);
    }
  }
}