﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace WordStack.Stacking.Knob
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/Knob/EventKnobsSO")]
  public class EventKnobsSO : ScriptableObject
  {
    public Action<IEnumerable<Knob>> Listeners = delegate { };

    public void Invoke(IEnumerable<Knob> value) => Listeners(value);
  }
}
