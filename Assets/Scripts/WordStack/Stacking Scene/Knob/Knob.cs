﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using MyBox;
using Utils;
using TMPro;

namespace WordStack.Stacking.Knob
{
  public class Knob : SceneComponent
  {
    [AutoProperty, Foldout("Components")] public SpriteRenderer Sprite = null;
    [AutoProperty, Foldout("Components")] public Rigidbody2D RB2D = null;
    [AutoProperty, Foldout("Components")] public TextMeshPro Text = null;
    [AutoProperty, Foldout("Components")] public Collider2D Collider = null;
    [AutoProperty, Foldout("Components")]
    public AudioEmitter AudioEmitter = null;
    public GameObjectPoolSO SharedDarkLayers = null;
    public SerializedObservable<SizeSettings> Size
      = new SerializedObservable<SizeSettings>(null);
    public SerializedObservable<ColorHDRSettings> Color
      = new SerializedObservable<ColorHDRSettings>(null);
    public SharedSettings Settings = null;

    void Awake()
    {
      Size.OnChange += ApplyScale;
      Color.OnChange += ApplyColor;
      if (Size.Value != null) ApplyScale(Size.Value);
      if (Color.Value != null) ApplyColor(Color.Value);
    }

    void OnEnable()
    {
      Sprite.SetAlpha(1f);
      gameObject.layer = Settings.DefaultLayer;
    }

    void OnDisable() => this.ToggleDarkLayer(false);

    public void ApplyScale(SizeSettings value) =>
      transform.localScale = value.Scale * Vector3.one;

    public void ApplyColor(ColorHDRSettings value) =>
      Sprite.material.color = value.Value;
  }
}

namespace WordStack.Stacking
{
  public static partial class Extensions
  {
    public static Knob.Knob CopyFrom(this Knob.Knob source, Knob.Knob original)
    {
      source.Size.Value = original.Size;
      source.Color.Value = original.Color;
      source.Text.text = original.Text.text;
      return source;
    }

    public static Knob.Knob ToggleDarkLayer(this Knob.Knob source, bool state)
    {
      var childDarkLayer = source.SharedDarkLayers.Value.Instances
        .FirstOrDefault(l => l.transform.parent == source.transform);
      if (childDarkLayer == null) childDarkLayer = source.SharedDarkLayers
        .Value.GetFirst(l => !l.activeSelf);
      if (state)
      {
        childDarkLayer.transform.SetParent(source.transform);
        childDarkLayer.transform.localScale = Vector3.one;
        childDarkLayer.transform.localPosition = Vector3.zero;
        childDarkLayer.SetActive(true);
      }
      else childDarkLayer?.PassTo(l => l.SetActive(false));
      return source;
    }
  }
}
