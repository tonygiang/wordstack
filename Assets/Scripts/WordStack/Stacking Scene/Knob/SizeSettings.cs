﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

namespace WordStack.Stacking.Knob
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/Knob/SizeSettings")]
  public class SizeSettings : ScriptableObject
  {
    public float Scale = 1f;
    public float ScoreMultiplier = 1;
    public RangedInt ParticlesEmittedOnPop = new RangedInt(1, 1);
  }
}
