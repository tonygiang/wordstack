﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace WordStack.Stacking
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/TutorialSettings")]
  public class TutorialSettings : ScriptableObject
  {
    public XMLDataFileSO SettingsFile = null;
    public string TutorialViewedQuery = string.Empty;
    public ToggleSO DontShowAgainToggle = null;
    public EventSO OnTutorialUnviewed = null;
    public EventSO OnTutorialClosed = null;

    public void CheckTutorialViewedStatus()
    {
      var tutViewedValue = SettingsFile.Document
        .SelectSingleNode(TutorialViewedQuery).Value;
      if (tutViewedValue == true.ToString()) OnTutorialClosed.Listeners();
      else OnTutorialUnviewed.Listeners();
    }

    public void SaveTutorialViewedSettings()
    {
      SettingsFile.Document.SelectSingleNode(TutorialViewedQuery)
        .Value = DontShowAgainToggle.Value.isOn.ToString();
      SettingsFile.Save();
    }
  }
}
