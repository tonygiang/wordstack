﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WordStack.Stacking
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/ScoreSettings")]
  public class ScoreSettings : ScriptableObject
  {
    public VariableInt Score = null;
    public int BaseKnobScore = 0;
    public float SameColorModifier = 0f;
  }
}
