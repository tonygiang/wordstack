﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;

namespace WordStack.Stacking
{
  public class Spawner : SceneComponent
  {
    public Collider2D SummonZone = null;
    public Knob.Pool KnobPool = null;

    void Awake()
    {
      SceneConfigs.SpawnSettings.SummonZone = SummonZone;
      SceneConfigs.SpawnSettings.KnobPool = KnobPool;
    }

    [ButtonMethod]
    public void Spawn()
    {
      if (SceneConfigs.SpawnSettings.IsLidActive.Value)
        StartCoroutine(SpawnCoroutine());
    }

    [ButtonMethod]
    public void Clear()
    {
      KnobPool.Instances.ForEach(k => k.gameObject.SetActive(false));
      SceneConfigs.WordSettings.ComposedClones.Clear();
    }

    IEnumerator SpawnCoroutine()
    {
      SceneConfigs.OnBeginKnobSpawn.Listeners();
      var charNumberSoFar = 0;
      var pickedWords = Enumerable.Empty<string>();
      var shuffledWords = SceneConfigs.WordSettings.ValidWords.Shuffle();
      foreach (string w in shuffledWords)
      {
        if (charNumberSoFar + w.Length >
          SceneConfigs.SpawnSettings.SpawningNumberRange.Max) break;
        charNumberSoFar += w.Length;
        pickedWords = pickedWords.Append(w);
      }
      var rolledChars = pickedWords.SelectMany(w => w.ToCharArray(),
        (w, c) => c);
      foreach (char c in rolledChars)
      {
        var newKnob = KnobPool.GetFirst(k => !k.gameObject.activeSelf);
        newKnob.Size.Value = SceneConfigs.SpawnSettings.KnobSizes.GetRandom();
        newKnob.Color.Value = SceneConfigs.SpawnSettings.KnobMaterialColors
          .GetRandom();
        newKnob.Text.text = c.ToString().ToUpper();
        newKnob.transform.position = SummonZone.bounds.RandomPoint();
        var darkLayerState = (SceneConfigs.WordSettings.ComposedClones.Count
          == SceneConfigs.WordSettings.WordLengthRange.Max);
        newKnob.ToggleDarkLayer(darkLayerState);
        newKnob.GetOrAddComponent<Knob.CloneOnMouseUpAsButton>();
        newKnob.gameObject.SetActive(true);
        newKnob.RB2D.velocity = Physics2D.gravity;
        yield return Utils.Unity.Constants
          .GetWaitForSeconds(SceneConfigs.SpawnSettings.SecondsBetweenSpawns);
      }
      SceneConfigs.OnKnobSpawnDone.Listeners();
      yield break;
    }
  }
}
