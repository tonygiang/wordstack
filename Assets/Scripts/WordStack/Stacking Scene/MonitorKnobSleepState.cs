﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace WordStack.Stacking
{
  public class MonitorKnobSleepState : SceneComponent
  {
    void OnEnable() => StartCoroutine(CheckKnobs());
    IEnumerable<Knob.Knob> _knobsToCheck => SceneConfigs.SpawnSettings.KnobPool
      .Instances.Where(k => k.gameObject.activeSelf)
      .Except(SceneConfigs.WordSettings.ComposedClones.Select(c => c.BaseComp))
      .Select(go => go.GetComponent<Knob.Knob>());

    IEnumerator CheckKnobs()
    {
      while (_knobsToCheck.Any(k => !k.RB2D.IsSleeping()))
        yield return Utils.Unity.Constants.WaitForFixedUpdate;
      SceneConfigs.OnAllKnobsAsleep.Listeners();
      yield break;
    }
  }
}
